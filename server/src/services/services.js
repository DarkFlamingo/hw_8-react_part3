import {
  user as UserRepository,
  message as MessageRepository,
} from '../data/repositories/repositories';

import { Message } from './message/message.service';
import { User } from './user/user.service';
import { Auth } from './auth/auth.service';

const user = new User(UserRepository);
const message = new Message(MessageRepository);
const auth = new Auth(UserRepository);

export { user, message, auth };
