class Auth {
  constructor(userRepository) {
    this._userRepository = userRepository;
  }

  login(data) {
    return this._userRepository.getUserByLoginAndPassword(data);
  }
}

export { Auth };
