class Message {
  constructor(messageRepository) {
    this._messageRepository = messageRepository;
  }

  getMessages() {
    return this._messageRepository.getAll();
  }

  getMessageById(id) {
    return this._messageRepository.getById(id);
  }

  create(data) {
    return this._messageRepository.create(data);
  }

  delete(id) {
    return this._messageRepository.deleteById(id);
  }

  update(id, data) {
    return this._messageRepository.updateById(id, data);
  }
}

export { Message };
