class User {
  constructor(userRepository) {
    this._userRepository = userRepository;
  }

  getUsers() {
    return this._userRepository.getAll();
  }

  getUserById(id) {
    return this._userRepository.getById(id);
  }

  create(data) {
    return this._userRepository.create(data);
  }

  delete(id) {
    return this._userRepository.deleteById(id);
  }

  update(id, data) {
    return this._userRepository.updateById(id, data);
  }
}

export { User };
