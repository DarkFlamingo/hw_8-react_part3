import User from './user/user.model';
import Message from './message/message.model';

export { User as UserModel, Message as MessageModel };
