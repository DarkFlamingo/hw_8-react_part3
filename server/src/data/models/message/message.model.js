import mongoose, { Schema, model } from 'mongoose';

const MessageSchema = new Schema({
  id: { type: mongoose.mongo.ObjectId },
  userId: { type: mongoose.mongo.ObjectId },
  avatar: { type: String },
  user: { type: String },
  text: { type: String },
  createdAt: { type: Date },
  editedAt: { type: Date },
});

export default model('Message', MessageSchema);
