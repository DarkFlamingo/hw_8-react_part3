import mongoose, { Schema, model } from 'mongoose';

const UserSchema = new Schema({
  id: { type: mongoose.mongo.ObjectId },
  login: { type: String, required: true },
  password: { type: String, required: true },
});

export default model('User', UserSchema);
