import { UserModel, MessageModel } from '../models';

import { User } from './user/user.repository';
import { Message } from './message/message.repository';

const message = new Message(MessageModel);

const user = new User(UserModel);

export { message, user };
