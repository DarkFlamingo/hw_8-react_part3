import { Abstract } from '../abstract/abstract.repository';

class User extends Abstract {
  constructor(userModel) {
    super(userModel);
  }

  addUser(user) {
    return this.create(user);
  }

  getUserByLoginAndPassword({ login, password }) {
    return this.model.findOne({ login, password });
  }
}

export { User };
