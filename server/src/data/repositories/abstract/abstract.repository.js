import mongoose from 'mongoose';

class Abstract {
  constructor(model) {
    this.model = model;
  }

  getAll() {
    return this.model.find({});
  }

  getById(id) {
    return this.model.findById(id);
  }

  create(data) {
    const item = new this.model({
      ...data,
      userId: mongoose.Types.ObjectId(data.userId),
    });
    return item.save();
  }

  deleteById(id) {
    return this.model.deleteOne({ _id: id });
  }

  updateById(id, data) {
    return this.model.findOneAndUpdate({ _id: id }, data);
  }
}

export { Abstract };
