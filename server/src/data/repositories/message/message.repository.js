import { Abstract } from '../abstract/abstract.repository';

class Message extends Abstract {
  constructor(messageModel) {
    super(messageModel);
  }

  addMessage(message) {
    return this.create(message);
  }
}

export { Message };
