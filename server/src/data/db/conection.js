import mongoose from 'mongoose';
import { CONNECTION_STRING } from '../../common/constants/constants';

const mongoDb = () =>
  mongoose.connect(CONNECTION_STRING, {
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

export { mongoDb };
