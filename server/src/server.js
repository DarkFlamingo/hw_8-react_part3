import express, { Router } from 'express';
import { ENV } from './common/enums/enums';
import { mongoDb } from './data/db/conection';
import { initApi } from './api/api';
import { errorHandler } from './middlewares/middlewares';
import cors from 'cors';

const app = express();

app.use(express.json());
app.use(cors());
app.use(ENV.APP.API_PATH, initApi(Router));
app.use(errorHandler);

mongoDb()
  .then(() => {
    console.info('Connection has been established successfully.');
    app.listen(ENV.APP.PORT, () => {
      console.info(`Server listening on port ${ENV.APP.PORT}`);
    });
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });
