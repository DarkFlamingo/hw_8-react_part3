const ApiPath = {
  MESSAGES: '/messages',
  USERS: '/users',
  LOGIN: '/login',
};

export { ApiPath };
