import { config } from 'dotenv';

config();

const { APP_PORT, SOCKET_PORT, SECRET_KEY } = process.env;

const ENV = {
  APP: {
    API_PATH: '/api',
    PORT: APP_PORT,
    SOCKET_PORT,
  },
  JWT: {
    SECRET: SECRET_KEY,
    EXPIRES_IN: '24h',
  },
};

export { ENV };
