import { config } from 'dotenv';

config();

const CONNECTION_STRING = process.env.CONNECTION_STRING;

export { CONNECTION_STRING };
