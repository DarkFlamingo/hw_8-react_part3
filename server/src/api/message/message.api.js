import { MessagesApiPath } from '../../common/enums/enums';

const initMessage = (Router, services) => {
  const { message: messageService } = services;
  const router = Router();

  router
    .get(MessagesApiPath.ROOT, (req, res, next) => {
      messageService
        .getMessages(req.query)
        .then((messages) => res.send(messages))
        .catch(next);
    })
    .get(MessagesApiPath.$ID, (req, res, next) => {
      messageService
        .getMessageById(req.params.id)
        .then((message) => res.send(message))
        .catch(next);
    })
    .post(MessagesApiPath.ROOT, (req, res, next) => {
      messageService
        .create(req.body)
        .then((message) => res.send(message))
        .catch(next);
    })
    .delete(MessagesApiPath.$ID, (req, res, next) => {
      messageService
        .delete(req.params.id)
        .then((count) => res.send({ count: count.n }))
        .catch(next);
    })
    .put(MessagesApiPath.$ID, (req, res, next) => {
      messageService
        .update(req.params.id, req.body)
        .then((message) => res.send(message))
        .catch(next);
    });

  return router;
};

export { initMessage };
