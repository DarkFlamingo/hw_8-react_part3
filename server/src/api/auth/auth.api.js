import { AuthApiPath } from '../../common/enums/enums';

const initAuth = (Router, services) => {
  const { auth: authService } = services;
  const router = Router();

  router.post(AuthApiPath.ROOT, (req, res, next) => {
    authService
      .login(req.body)
      .then((user) => {
        return res.send(user);
      })
      .catch(next);
  });

  return router;
};

export { initAuth };
