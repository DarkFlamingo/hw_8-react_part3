import { ApiPath } from '../common/enums/enums';
import { user, message, auth } from '../services/services';
import { initUser } from './user/user.api';
import { initMessage } from './message/message.api';
import { initAuth } from './auth/auth.api';

const initApi = (Router) => {
  const apiRouter = Router();

  apiRouter.use(ApiPath.USERS, initUser(Router, { user }));
  apiRouter.use(ApiPath.MESSAGES, initMessage(Router, { message }));
  apiRouter.use(ApiPath.LOGIN, initAuth(Router, { auth }));

  return apiRouter;
};

export { initApi };
