import { UsersApiPath } from '../../common/enums/enums';

const initUser = (Router, services) => {
  const { user: UserService } = services;
  const router = Router();

  router
    .get(UsersApiPath.ROOT, (req, res, next) => {
      UserService.getUsers(req.query)
        .then((users) => res.send(users))
        .catch(next);
    })
    .get(UsersApiPath.$ID, (req, res, next) => {
      UserService.getUserById(req.params.id)
        .then((user) => res.send(user))
        .catch(next);
    })
    .post(UsersApiPath.ROOT, (req, res, next) => {
      UserService.create(req.body)
        .then((user) => res.send(user))
        .catch(next);
    })
    .put(UsersApiPath.$ID, (req, res, next) => {
      UserService.update(req.params.id, req.body)
        .then((user) => res.send(user))
        .catch(next);
    })
    .delete(UsersApiPath.$ID, (req, res, next) => {
      UserService.delete(req.params.id)
        .then((count) => res.send({ count: count.n }))
        .catch(next);
    });

  return router;
};

export { initUser };
