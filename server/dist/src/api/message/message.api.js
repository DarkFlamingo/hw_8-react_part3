import { MessagesApiPath } from '../../common/enums/enums';

const initMessage = (Router, services) => {
  const { message: messageService } = services;
  const router = Router();

  router.get(MessagesApiPath.ROOT, (req, res, next) => {
    messageService.getMessages(req.query).then(messages => res.send(messages)).catch(next);
  }).get(MessagesApiPath.$ID, (req, res, next) => {
    messageService.getMessageById(req.params.id).then(message => res.send(message)).catch(next);
  }).post(MessagesApiPath.ROOT, (req, res, next) => {
    messageService.addMessage(req.body).then(message => res.send(message)).catch(next);
  }).put(MessagesApiPath.$ID, (req, res, next) => {
    messageService.updateMessage(req.params.id, req.body).then(message => res.send(message)).catch(next);
  }).delete(MessagesApiPath.$ID, (req, res, next) => {
    messageService.deleteMessage(req.params.id).then(count => res.send({ count })).catch(next);
  });

  return router;
};

export { initMessage };