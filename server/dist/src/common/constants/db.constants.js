import { config } from 'dotenv';

config();

const CONNECTION_STRING = process.env;

export { CONNECTION_STRING };