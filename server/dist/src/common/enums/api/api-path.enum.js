const ApiPath = {
  MESSAGES: '/messages',
  USER: '/users'
};

export { ApiPath };