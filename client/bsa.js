import Chat from './src/components/chat/chat';
import { rootReducer } from './src/store/root-reducer';

export default {
  Chat,
  rootReducer,
};
