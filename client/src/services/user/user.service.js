import { HttpMethod, ContentType } from '../../common/enums/enums';

class User {
  constructor({ http }) {
    this._http = http;
  }

  getAllUsers() {
    return this._http.load('/api/users', {
      method: HttpMethod.GET,
    });
  }

  getUserById(id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.GET,
    });
  }

  addUser(user) {
    return this._http.load('/api/users', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(user),
    });
  }

  deleteUser(id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.DELETE,
    });
  }

  updateUser({ id, ...data }) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(data),
    });
  }
}

export { User };
