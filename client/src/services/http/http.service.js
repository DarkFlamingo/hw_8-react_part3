import { stringify } from 'query-string';
import { HttpHeader, HttpMethod } from '../../common/enums/enums';
import { SERVER_URL } from '../../common/constants/constants';

class Http {
  load(url, options = {}) {
    const {
      method = HttpMethod.GET,
      payload = null,
      contentType,
      query,
    } = options;
    const headers = this._getHeaders({
      contentType,
    });

    return fetch(this._getUrl(url, query), {
      method,
      headers,
      body: payload,
    })
      .then(this._parseJSON)
      .catch(this._throwError);
  }

  _getHeaders({ contentType }) {
    const headers = new Headers();

    if (contentType) {
      headers.append(HttpHeader.CONTENT_TYPE, contentType);
    }

    return headers;
  }

  _getUrl(url, query) {
    console.log(`${SERVER_URL}${url}${query ? `?${stringify(query)}` : ''}`);
    return `${SERVER_URL}${url}${query ? `?${stringify(query)}` : ''}`;
  }

  _parseJSON(response) {
    return response.json();
  }

  _throwError(err) {
    throw err;
  }
}

export { Http };
