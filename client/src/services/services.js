import { Auth } from './auth/auth.service';
import { User } from './user/user.service';
import { Message } from './message/message.service';
import { Http } from './http/http.service';
import { Storage } from './storage/storage.service';

const storage = new Storage({
  storage: localStorage,
});

const http = new Http();

const user = new User({ http });

const message = new Message({ http });

const auth = new Auth({ http });

export { user, message, auth, storage };
