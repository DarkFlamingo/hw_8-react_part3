import { HttpMethod, ContentType } from '../../common/enums/enums';

class Message {
  constructor({ http }) {
    this._http = http;
  }

  getAllMessages() {
    return this._http.load('/api/messages', {
      method: HttpMethod.GET,
    });
  }

  getMessageById(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.GET,
    });
  }

  addMessage(message) {
    return this._http.load('/api/messages', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(message),
    });
  }

  deleteMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.DELETE,
    });
  }

  updateMessage({ id, ...data }) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(data),
    });
  }
}

export { Message };
