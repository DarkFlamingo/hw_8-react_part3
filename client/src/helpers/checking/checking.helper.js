const checkUserIsAdmin = (user) => {
  return user.password === 'admin' && user.login === 'admin';
};

export { checkUserIsAdmin };
