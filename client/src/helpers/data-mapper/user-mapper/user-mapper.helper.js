const userArrayMapper = (data) => {
  return data.map((item) => userMapper(item));
};

const userMapper = (data) => {
  console.log(data);
  return {
    id: data._id,
    login: data.login,
    password: data.password,
  };
};

export { userArrayMapper, userMapper };
