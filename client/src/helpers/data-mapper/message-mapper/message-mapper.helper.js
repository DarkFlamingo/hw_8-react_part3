const messageArrayMapper = (data) => {
  return data.map((item) => messageMapper(item));
};

const messageMapper = (data) => ({
  id: data._id,
  userId: data.userId,
  avatar: data.avatar,
  user: data.user,
  text: data.text,
  createdAt: data.createdAt,
  editedAt: data.editedAt,
});

export { messageMapper, messageArrayMapper };
