import './style.css';
import React from 'react';
import { Button } from '../common/common';
import { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { AppRoute } from '../../common/enums/enums';
import { useSelector, useDispatch } from 'react-redux';
import { usersActionCreator } from '../../store/actions';

const EditUser = () => {
  const { editableUser } = useSelector((state) => ({
    editableUser: state.userEditor.editableUser,
  }));

  const dispatch = useDispatch();

  const updateUser = React.useCallback(
    async (user) => dispatch(usersActionCreator.changeUser(user)),
    [dispatch]
  );

  const [login, setLogin] = useState(editableUser.login);
  const [redirect, setRedirect] = useState('');

  const onClosed = () => setRedirect(AppRoute.USERS_LIST);

  const loginChanged = (data) => {
    setLogin(data);
  };

  const handleSave = async () => {
    await updateUser({ ...editableUser, login: login });
    setRedirect(AppRoute.USERS_LIST);
  };

  return !redirect ? (
    <div className="edit-user-modal">
      <div className="col-3 modal-wrapper">
        <div className="modal-title">Edit User</div>
        <input
          type="text"
          onChange={(ev) => loginChanged(ev.target.value)}
          value={login}
          className="edit-user-input"
        />
        <div className="modal-btn">
          <Button
            className="edit-user-close"
            variant="secondary"
            onClick={onClosed}
          >
            Close
          </Button>
          <Button
            className="edit-user-button"
            variant="primary"
            onClick={handleSave}
          >
            Save Changes
          </Button>
        </div>
      </div>
    </div>
  ) : (
    <Redirect to={redirect} />
  );
};

export default EditUser;
