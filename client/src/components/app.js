import './reset.css';
import './style.css';
import { useSelector, useDispatch } from 'react-redux';
import React from 'react';
import { Switch } from 'react-router-dom';
import Login from './login/login';
import Chat from './chat/chat';
import MainNavbar from './main-navbar/main-navbar';
import UserPage from './user-page/user-page';
import EditMessage from './edit-message/edit-message';
import EditUser from './edit-user/edit-user';
import Footer from './footer/footer';
import { PublicRoute, PrivateRoute, AdminRoute } from './common/common';
import { AppRoute, StorageKey } from '../common/enums/enums';
import { storage } from '../services/services';
import { profileActionCreator } from '../store/actions';
import { Preloader } from './common/common';

const App = () => {
  const { currentUser, isAdmin } = useSelector((state) => ({
    currentUser: state.profile.currentUser,
    isAdmin: state.profile.isAdmin,
  }));
  const dispatch = useDispatch();

  const hasUserId = Boolean(storage.getItem(StorageKey.USER_ID));
  const hasUser = Boolean(currentUser);

  const handleLoadCurrentUser = React.useCallback(async () => {
    await dispatch(profileActionCreator.loadCurrentUser());
  }, [dispatch]);

  const handleLogOut = React.useCallback(async () => {
    await dispatch(profileActionCreator.logout());
  }, [dispatch]);

  React.useEffect(() => {
    if (hasUserId) {
      handleLoadCurrentUser();
    }
  }, [hasUserId, handleLoadCurrentUser]);

  if (!hasUser && hasUserId) {
    <Preloader />;
  }

  return (
    <div className="main-wrapper">
      {hasUser && <MainNavbar isAdmin={isAdmin} onLogOut={handleLogOut} />}
      <Switch>
        <PrivateRoute exact path={[AppRoute.ROOT]} component={Chat} />
        <AdminRoute exact path={[AppRoute.USERS_LIST]} component={UserPage} />
        <AdminRoute exact path={[AppRoute.USER_EDITOR]} component={EditUser} />
        <PrivateRoute
          exact
          path={[AppRoute.MESSAGE_EDITOR]}
          component={EditMessage}
        />
        <PublicRoute exact path={[AppRoute.LOGIN]} component={Login} />
      </Switch>
      <Footer />
    </div>
  );
};

export default App;
