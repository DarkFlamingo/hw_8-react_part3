import './style.css';
import React from 'react';
import { Button } from '../common/common';
import { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { AppRoute } from '../../common/enums/enums';
import { useSelector, useDispatch } from 'react-redux';
import { chatActionCreator } from '../../store/actions';

const EditMessage = () => {
  const { editableMessage } = useSelector((state) => ({
    editableMessage: state.messageEditor.editableMessage,
  }));

  const dispatch = useDispatch();

  const updateMessage = React.useCallback(
    async (message) => dispatch(chatActionCreator.changeMessage(message)),
    [dispatch]
  );

  const [text, setText] = useState(editableMessage.text);
  const [redirect, setRedirect] = useState('');

  const onClosed = () => setRedirect(AppRoute.ROOT);

  const textChanged = (data) => {
    setText(data);
  };

  const handleSave = async () => {
    await updateMessage({ ...editableMessage, text: text });
    setRedirect(AppRoute.ROOT);
  };

  return !redirect ? (
    <div className="edit-message-modal">
      <div className="col-3 modal-wrapper">
        <div className="modal-title">Edit Message</div>
        <input
          type="text"
          onChange={(ev) => textChanged(ev.target.value)}
          value={text}
          className="edit-message-input"
        />
        <div className="modal-btn">
          <Button
            className="edit-message-close"
            variant="secondary"
            onClick={onClosed}
          >
            Close
          </Button>
          <Button
            className="edit-message-button"
            variant="primary"
            onClick={handleSave}
          >
            Save Changes
          </Button>
        </div>
      </div>
    </div>
  ) : (
    <Redirect to={redirect} />
  );
};

export default EditMessage;
