import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { AppRoute } from '../../../common/enums/enums';

const AdminRoute = ({ component: Component, ...rest }) => {
  const { currentUser, isAdmin } = useSelector((state) => ({
    currentUser: state.profile.currentUser,
    isAdmin: state.profile.isAdmin,
  }));

  const hasUser = Boolean(currentUser);

  return (
    <Route
      {...rest}
      render={(props) =>
        hasUser && isAdmin ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: AppRoute.LOGIN, state: { from: props.location } }}
          />
        )
      }
    />
  );
};

export default AdminRoute;
