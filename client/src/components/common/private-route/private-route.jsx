import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { AppRoute } from '../../../common/enums/enums';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { currentUser } = useSelector((state) => ({
    currentUser: state.profile.currentUser,
  }));

  const hasUser = Boolean(currentUser);

  return (
    <Route
      {...rest}
      render={(props) =>
        hasUser ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: AppRoute.LOGIN, state: { from: props.location } }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
