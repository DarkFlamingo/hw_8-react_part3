import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { AppRoute } from '../../../common/enums/enums';

const PublicRoute = ({ component: Component, ...rest }) => {
  const { currentUser } = useSelector((state) => ({
    currentUser: state.profile.currentUser,
  }));

  const hasUser = Boolean(currentUser);

  return (
    <Route
      {...rest}
      render={(props) =>
        hasUser ? (
          <Redirect
            to={{ pathname: AppRoute.ROOT, state: { from: props.location } }}
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

export default PublicRoute;
