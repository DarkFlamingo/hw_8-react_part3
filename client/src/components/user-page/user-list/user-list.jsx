import './style.css';
import User from './user/user';

const UserList = ({ users, onDelete, onEdit }) => {
  const usersItem = users.map((user) => (
    <User user={user} onDelete={onDelete} onEdit={onEdit} key={user.id} />
  ));

  return <div className="user-list">{usersItem}</div>;
};

export default UserList;
