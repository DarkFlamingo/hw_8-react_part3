import './style.css';
import { Button } from '../../../common/common';

const User = ({ user, onDelete, onEdit }) => {
  const handleOnEdit = (user) => {
    onEdit(user);
  };

  const handleOnDelete = (id) => {
    onDelete(id);
  };

  return (
    <div className="user-item">
      <div className="user-login">{user.login}</div>
      <Button
        className="user-delete-btn"
        variant="danger"
        onClick={() => handleOnDelete(user.id)}
      >
        Delete
      </Button>
      <Button
        className="user-update-btn"
        variant="warning"
        onClick={() => handleOnEdit(user)}
      >
        Update
      </Button>
    </div>
  );
};

export default User;
