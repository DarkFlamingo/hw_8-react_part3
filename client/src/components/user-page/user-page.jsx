import './style.css';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from '../common/common';
import UserList from './user-list/user-list';
import { Preloader } from '../common/common';
import {
  usersActionCreator,
  userEditorActionCreator,
} from '../../store/actions';
import { Redirect } from 'react-router-dom';
import { AppRoute } from '../../common/enums/enums';

const UserPage = () => {
  const { users, preloader, currentUser } = useSelector((state) => ({
    preloader: state.usersPage.preloader,
    users: state.usersPage.users,
    currentUser: state.profile.currentUser,
  }));
  const dispatch = useDispatch();

  const [redirect, setRedirect] = React.useState('');

  const handleLoadUsers = React.useCallback(
    () => dispatch(usersActionCreator.loadUsers()),
    [dispatch]
  );

  const handleUserDelete = React.useCallback(
    (id) => dispatch(usersActionCreator.removeUser(id)),
    [dispatch]
  );

  const handleSetUserForEditing = React.useCallback(
    (user) => dispatch(userEditorActionCreator.setEditableUser(user)),
    [dispatch]
  );

  const filterUsers = (users) =>
    users.filter((user) => user.id !== currentUser.id);

  React.useEffect(() => {
    handleLoadUsers();
  }, [handleLoadUsers]);

  const editUser = (user) => {
    handleSetUserForEditing(user);
    setRedirect(AppRoute.USER_EDITOR);
  };

  return !redirect ? (
    <div className="user-page">
      {preloader ? (
        <Preloader />
      ) : (
        <div className="container">
          <UserList
            users={filterUsers(users)}
            onDelete={handleUserDelete}
            onEdit={editUser}
          />
        </div>
      )}
    </div>
  ) : (
    <Redirect to={redirect} />
  );
};

export default UserPage;
