import './style.css';
import { Button, Image } from '../../../../common/common';
import { MESSSAGE_DATE_FORMAT } from '../../../../../common/constants/constants';
import React, { useState } from 'react';
import { Heart, HeartFill } from 'react-bootstrap-icons';
import moment from 'moment';

const Message = ({ message }) => {
  const [isLiked, toggleIsLiked] = useState(false);

  const handleLike = () => {
    toggleIsLiked(true);
  };

  const handleDislike = () => {
    toggleIsLiked(false);
  };

  return (
    <div className="message col-8">
      <Image src={message.avatar} className="message-user-avatar"></Image>
      <p className="message-user-name">{message.user}</p>
      <p className="message-text">{message.text}</p>
      <div className="group">
        {isLiked ? (
          <Button
            variant="outline-danger"
            className="message-liked"
            onClick={handleDislike}
          >
            <HeartFill color="red"/>
          </Button>
        ) : (
          <Button
            variant="outline-danger"
            className="message-like"
            onClick={handleLike}
          >
            <Heart color="red"/>
          </Button>
        )}
        <p className="message-time">
          {moment(message.createdAt).format(MESSSAGE_DATE_FORMAT)}
        </p>
      </div>
    </div>
  );
};

export default Message;
