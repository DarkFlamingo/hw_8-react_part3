import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import MessageWrapper from './message-wrapper/message-wrapper';
import React, { useEffect } from 'react';
import { Preloader } from '../common/common';
import {
  USER_DEFAULT_URL,
  ARROW_UP_CODE,
} from '../../common/constants/constants';
import { useSelector, useDispatch } from 'react-redux';
import {
  chatActionCreator,
  messageEditorActionCreator,
} from '../../store/actions';
import { AppRoute } from '../../common/enums/enums';
import { Redirect } from 'react-router-dom';

const Chat = () => {
  const { messages, preloader, lastCreatedMessage, me } = useSelector(
    (state) => ({
      messages: state.chat.messages,
      preloader: state.chat.preloader,
      lastCreatedMessage: state.chat.lastCreatedMessage,
      me: state.profile.currentUser,
    })
  );

  const [redirect, setRedirect] = React.useState('');

  const dispatch = useDispatch();

  const handleLoadMessages = React.useCallback(
    () => dispatch(chatActionCreator.loadMessages()),
    [dispatch]
  );

  const handleMessageAdd = React.useCallback(
    (message) => dispatch(chatActionCreator.createNewMessage(message)),
    [dispatch]
  );

  const hadleMessageDelete = React.useCallback(
    (id) => dispatch(chatActionCreator.removeMessage(id)),
    [dispatch]
  );

  const handleSetMessageForEditing = React.useCallback(
    (message) =>
      dispatch(messageEditorActionCreator.setEditableMessage(message)),
    [dispatch]
  );

  useEffect(() => {
    handleLoadMessages();
  }, [handleLoadMessages]);

  const addMessage = (text) => {
    if (text) {
      const messageToAdd = {
        userId: me.id,
        avatar: USER_DEFAULT_URL,
        user: me.login,
        text,
        createdAt: new Date(Date.now()).toISOString(),
        editedAt: '',
      };
      handleMessageAdd(messageToAdd);
    }
  };

  const deleteMessage = (id) => {
    hadleMessageDelete(id);
  };

  const editMessage = (message) => {
    handleSetMessageForEditing(message);
    setRedirect(AppRoute.MESSAGE_EDITOR);
  };

  const handleKeyDown = (event) => {
    if (event.code === ARROW_UP_CODE) {
      if (lastCreatedMessage !== null) {
        editMessage(lastCreatedMessage);
      }
    }
  };

  return !redirect ? (
    <div className="chat" tabIndex="0" onKeyDown={handleKeyDown}>
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <MessageWrapper
            messages={messages}
            me={me}
            addMessage={addMessage}
            deleteMessage={deleteMessage}
            editMessage={editMessage}
          />
        </>
      )}
    </div>
  ) : (
    <Redirect to={redirect} />
  );
};

export default Chat;
