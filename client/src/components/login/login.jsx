import './style.css';
import React from 'react';
import { profileActionCreator } from '../../store/actions';
import { DataStatus } from '../../common/enums/enums';
import { useDispatch, useSelector } from 'react-redux';
import LoginForm from './login-form/login-form';
import { Preloader } from '../common/common';

const Login = () => {
  const { status } = useSelector((state) => ({
    status: state.profile.status,
    currentUser: state.profile.currentUser,
  }));
  const dispatch = useDispatch();

  const handleLogin = React.useCallback(
    async (loginPayload) => {
      await dispatch(profileActionCreator.login(loginPayload));
    },
    [dispatch]
  );

  return status === DataStatus.PENDING ? (
    <div className="preloader-wrp">
      <Preloader />
    </div>
  ) : (
    <div className="login-wrapper">
      <LoginForm onLogin={handleLogin} isError={status === DataStatus.ERROR} />
    </div>
  );
};

export default Login;
