import './style.css';
import { useState } from 'react';
import { Button, Form } from '../../common/common';

const LoginForm = ({ onLogin, isError }) => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const loginChanged = (data) => {
    setLogin(data);
  };

  const passwordChanged = (data) => {
    setPassword(data);
  };

  const handleSignInClick = (event) => {
    onLogin({ login, password });
    event.preventDefault();
  };

  return (
    <Form className="login-form-wrapper" onSubmit={handleSignInClick}>
      <Form.Group className="login-form-group">
        <div className="login-title">Login</div>
        <Form.Control
          className="login-form-input-login"
          onChange={(ev) => loginChanged(ev.target.value)}
          value={login}
        ></Form.Control>
        <Form.Control
          className="login-form-input-password"
          onChange={(ev) => passwordChanged(ev.target.value)}
          value={password}
          type="password"
        ></Form.Control>
        {isError && <div className="error-message">Invalid data</div>}
        <Button className="login-button" variant="primary" type="submit">
          Sign In
        </Button>
      </Form.Group>
    </Form>
  );
};

export default LoginForm;
