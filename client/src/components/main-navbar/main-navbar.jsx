import './style.css';
import { Nav, Navbar, Image, Button } from '../common/common';
import { NavLink } from 'react-router-dom';

const MainNavbar = ({ isAdmin, onLogOut }) => {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand className="logo-wrapper">
        <Image className="main-header__logo" src="/logo.jpg" alt="Logo:(" />
        <span className="main-header__name">Chatalk</span>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavLink to="/" className="navbar-link">
            Chat
          </NavLink>
          {isAdmin && (
            <NavLink to="/users" className="navbar-link">
              Users
            </NavLink>
          )}
        </Nav>
      </Navbar.Collapse>
      <Button className="btn-logout" onClick={onLogOut} size="sm">
        Log Out
      </Button>
    </Navbar>
  );
};

export default MainNavbar;
