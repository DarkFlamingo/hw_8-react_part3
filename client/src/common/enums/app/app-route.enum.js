const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  MESSAGE_EDITOR: '/editor',
  USERS_LIST: '/users',
  USER_EDITOR: '/users/editor',
};

export { AppRoute };
