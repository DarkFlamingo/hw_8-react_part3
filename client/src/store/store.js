import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from './root-reducer';
import { thunkMiddleware } from './middleware';

const store = configureStore({
  reducer: rootReducer,
  middleware: thunkMiddleware,
});

window.store = store;

export default store;
