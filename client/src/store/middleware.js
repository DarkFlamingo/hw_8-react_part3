import {
  user as UserService,
  auth as AuthService,
  message as MessageService,
  storage as StorageService,
} from '../services/services';

const thunkMiddleware = (getDefaultMiddleware) => {
  return getDefaultMiddleware({
    thunk: {
      extraArgument: {
        UserService,
        MessageService,
        AuthService,
        StorageService,
      },
    },
  });
};

export { thunkMiddleware };
