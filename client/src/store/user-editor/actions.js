import { createAction } from '@reduxjs/toolkit';

const ActionType = {
  SET_EDITABLE_USER: 'user-editor/set-editable-user',
};

const setEditableUser = createAction(ActionType.SET_EDITABLE_USER, (user) => ({
  payload: {
    user,
  },
}));

export { setEditableUser };
