import { createReducer } from '@reduxjs/toolkit';
import { setEditableUser } from './actions';

const initialState = {
  editableUser: null,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setEditableUser, (state, action) => {
    const { user } = action.payload;

    state.editableUser = user;
  });
});

export { reducer };
