import { createAction } from '@reduxjs/toolkit';
import { messageMapper, messageArrayMapper } from '../../helpers/helpers';
import { message as MessageService } from '../../services/services';

const ActionType = {
  SET_MESSAGES: 'chat/set-messages',
  ADD_MESSAGE: 'chat/add-message',
  DELETE_MESSAGE: 'chat/delete-message',
  UPDATE_MESSAGE: 'chat/update-message',
  SET_PRELOADER: 'chat/set-preloader',
  SET_LAST_CREATED_MESSAGE: 'chat/set-last-created-message',
};

const setMessages = createAction(ActionType.SET_MESSAGES, (messages) => ({
  payload: {
    messages,
  },
}));

const addMessage = createAction(ActionType.ADD_MESSAGE, (message) => ({
  payload: {
    message,
  },
}));

const deleteMessage = createAction(ActionType.DELETE_MESSAGE, (id) => ({
  payload: {
    id,
  },
}));

const updateMessage = createAction(ActionType.UPDATE_MESSAGE, (message) => ({
  payload: {
    message,
  },
}));

const setPreloader = createAction(ActionType.SET_PRELOADER, (preloader) => ({
  payload: {
    preloader,
  },
}));

const setLastCreatedMessage = createAction(
  ActionType.SET_LAST_CREATED_MESSAGE,
  (message) => ({
    payload: {
      message,
    },
  })
);

const loadMessages = () => async (dispatch) => {
  dispatch(setPreloader(true));
  const messages = await MessageService.getAllMessages();
  dispatch(setMessages(messageArrayMapper(messages)));
  dispatch(setPreloader(false));
};

const createNewMessage = (message) => async (dispatch) => {
  const newMessage = await MessageService.addMessage(message);
  dispatch(addMessage(messageMapper(newMessage)));
  dispatch(setLastCreatedMessage(messageMapper(newMessage)));
};

const removeMessage = (id) => async (dispatch) => {
  const { count } = await MessageService.deleteMessage(id);
  if (count !== 0) {
    dispatch(deleteMessage(id));
  }
};

const changeMessage = (message) => async (dispatch) => {
  const updatedMessage = await MessageService.updateMessage(message);
  dispatch(updateMessage(messageMapper(updatedMessage)));
};

export {
  setMessages,
  addMessage,
  deleteMessage,
  updateMessage,
  setPreloader,
  setLastCreatedMessage,
  loadMessages,
  createNewMessage,
  removeMessage,
  changeMessage,
};
