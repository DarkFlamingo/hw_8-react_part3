import { createReducer } from '@reduxjs/toolkit';
import {
  setMessages,
  addMessage,
  deleteMessage,
  updateMessage,
  setPreloader,
  setLastCreatedMessage,
} from './actions';

const initialState = {
  messages: [],
  preloader: true,
  lastCreatedMessage: null,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setMessages, (state, action) => {
    const { messages } = action.payload;

    state.messages = messages;
  });
  builder.addCase(addMessage, (state, action) => {
    const { message } = action.payload;

    state.messages = [...state.messages, message];
  });
  builder.addCase(deleteMessage, (state, action) => {
    const { id } = action.payload;

    state.messages = state.messages.filter((message) => message.id !== id);
  });
  builder.addCase(updateMessage, (state, action) => {
    const { message } = action.payload;

    state.messages = state.messages.map((item) =>
      item.id === message.id ? message : item
    );
  });
  builder.addCase(setPreloader, (state, action) => {
    const { preloader } = action.payload;

    state.preloader = preloader;
  });
  builder.addCase(setLastCreatedMessage, (state, action) => {
    const { message } = action.payload;

    state.lastCreatedMessage = message;
  });
});

export { reducer };
