import { createAction } from '@reduxjs/toolkit';
import { userMapper, userArrayMapper } from '../../helpers/helpers';
import { user as UserService } from '../../services/services';

const AcitonType = {
  SET_USERS: 'users/set-users',
  DELETE_USER: 'users/delete-user',
  ADD_USER: 'users/add-user',
  UPDATE_USER: 'users/update-user',
  SET_PRELOADER: 'users/set-preloader',
};

const setUsers = createAction(AcitonType.SET_USERS, (users) => ({
  payload: {
    users,
  },
}));

const deleteUser = createAction(AcitonType.DELETE_USER, (id) => ({
  payload: {
    id,
  },
}));

const addUser = createAction(AcitonType.ADD_USER, (user) => ({
  payload: {
    user,
  },
}));

const updateUser = createAction(AcitonType.UPDATE_USER, (user) => ({
  payload: {
    user,
  },
}));

const setPreloader = createAction(AcitonType.SET_PRELOADER, (preloader) => ({
  payload: {
    preloader,
  },
}));

const loadUsers = () => async (dispatch) => {
  dispatch(setPreloader(true));
  const users = await UserService.getAllUsers();
  dispatch(setUsers(userArrayMapper(users)));
  dispatch(setPreloader(false));
};

const createNewUser = (user) => async (dispatch) => {
  const newUser = await UserService.addUser(user);
  dispatch(addUser(userMapper(newUser)));
};

const removeUser = (id) => async (dispatch) => {
  const { count } = await UserService.deleteUser(id);
  if (count !== 0) {
    dispatch(deleteUser(id));
  }
};

const changeUser = (user) => async (dispatch) => {
  const updatedUser = await UserService.updateUser(user);
  dispatch(updateUser(userMapper(updatedUser)));
};

export {
  setUsers,
  deleteUser,
  addUser,
  updateUser,
  setPreloader,
  loadUsers,
  createNewUser,
  removeUser,
  changeUser,
};
