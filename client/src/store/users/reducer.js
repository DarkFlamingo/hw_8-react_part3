import { createReducer } from '@reduxjs/toolkit';
import {
  setUsers,
  deleteUser,
  addUser,
  updateUser,
  setPreloader,
} from './actions';

const initialState = {
  users: [],
  preloader: true,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setUsers, (state, action) => {
    const { users } = action.payload;

    state.users = users;
  });
  builder.addCase(deleteUser, (state, action) => {
    const { id } = action.payload;

    state.users = state.users.filter((user) => user.id !== id);
  });
  builder.addCase(addUser, (state, action) => {
    const { user } = action.payload;

    state.users = [...state.users, user];
  });
  builder.addCase(updateUser, (state, action) => {
    const { user } = action.payload;

    state.users = state.users.map((item) =>
      item.id === user.id ? user : item
    );
  });
  builder.addCase(setPreloader, (state, action) => {
    const { preloader } = action.payload;

    state.preloader = preloader;
  });
});

export { reducer };
