import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { StorageKey } from '../../common/enums/enums';
import { userMapper, checkUserIsAdmin } from '../../helpers/helpers';

const ActionType = {
  SET_CURRENT_USER: 'profile/set-current-user',
  LOAD_CURRENT_USER: 'profile/load-current-user',
  LOGIN: 'profile/login',
  LOGOUT: 'profile/logout',
};

const setCurrentUser = createAction(ActionType.SET_CURRENT_USER, (user) => ({
  payload: { user },
}));

const logout = createAsyncThunk(
  ActionType.LOGOUT,
  async (payload, { extra }) => {
    extra.StorageService.removeItem(StorageKey.USER_ID);
    return {
      user: null,
      isAdmin: false,
    };
  }
);

const login = createAsyncThunk(ActionType.LOGIN, async (payload, { extra }) => {
  const user = await extra.AuthService.login(payload);
  extra.StorageService.setItem(StorageKey.USER_ID, user._id);
  return user
    ? {
        user: userMapper(user),
        isAdmin: checkUserIsAdmin(user),
      }
    : {
        user: null,
        isAdmin: false,
      };
});

const loadCurrentUser = createAsyncThunk(
  ActionType.LOAD_CURRENT_USER,
  async (payload, { extra }) => {
    const user = await extra.UserService.getUserById(
      extra.StorageService.getItem(StorageKey.USER_ID)
    );
    return { user: userMapper(user), isAdmin: checkUserIsAdmin(user) };
  }
);

export { setCurrentUser, login, logout, loadCurrentUser };
