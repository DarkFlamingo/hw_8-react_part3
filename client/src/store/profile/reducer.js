import { createReducer } from '@reduxjs/toolkit';
import { setCurrentUser, login, logout, loadCurrentUser } from './actions';
import { DataStatus } from '../../common/enums/enums';

const initialState = {
  currentUser: null,
  isAdmin: false,
  status: DataStatus.IDLE,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setCurrentUser, (state, action) => {
    const { user } = action.payload;

    state.currentUser = user;
  });
  builder.addCase(login.pending, (state) => {
    state.status = DataStatus.PENDING;
  });
  builder.addCase(login.fulfilled, (state, { payload }) => {
    const { user, isAdmin } = payload;

    state.currentUser = user;
    state.status = DataStatus.SUCCESS;
    state.isAdmin = isAdmin;
  });
  builder.addCase(login.rejected, (state) => {
    state.status = DataStatus.ERROR;
  });
  builder.addCase(logout.fulfilled, (state, { payload }) => {
    const { user, isAdmin } = payload;

    state.currentUser = user;
    state.isAdmin = isAdmin;
    state.status = DataStatus.IDLE;
  });
  builder.addCase(loadCurrentUser.fulfilled, (state, { payload }) => {
    const { user, isAdmin } = payload;

    state.currentUser = user;
    state.status = DataStatus.SUCCESS;
    state.isAdmin = isAdmin;
  });
});

export { reducer };
