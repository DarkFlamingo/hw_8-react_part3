import { createAction } from '@reduxjs/toolkit';

const ActionType = {
  SET_EDITABLE_MESSAGE: 'message-editor/set-editable-message',
};

const setEditableMessage = createAction(
  ActionType.SET_EDITABLE_MESSAGE,
  (message) => ({
    payload: {
      message,
    },
  })
);

export { setEditableMessage };
