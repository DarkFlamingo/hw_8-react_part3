import { createReducer } from '@reduxjs/toolkit';
import { setEditableMessage } from './actions';

const initialState = {
  editableMessage: null,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setEditableMessage, (state, action) => {
    const { message } = action.payload;

    state.editableMessage = message;
  });
});

export { reducer };
