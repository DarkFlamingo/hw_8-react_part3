import { reducer as chatReducer } from './chat/reducer';
import { reducer as profileReducer } from './profile/reducer';
import { reducer as messageEditorReducer } from './message-editor/reducer';
import { reducer as usersEditorReducer } from './users/reducer';
import { reducer as userEditorReducer } from './user-editor/reducer';
import { combineReducers } from '@reduxjs/toolkit';

const rootReducer = combineReducers({
  chat: chatReducer,
  profile: profileReducer,
  messageEditor: messageEditorReducer,
  usersPage: usersEditorReducer,
  userEditor: userEditorReducer,
});

export { rootReducer };
